<%@ include file="/WEB-INF/core/fragment/common.jspf" %>
<html>
<head>
<link rel="stylesheet" href="${coreCssURL}/core.css"/>
<link rel="stylesheet" href="${coreCssURL}/search.css"/>
<link rel="stylesheet" href="${coreCssURL}/tag.css"/>
<script src="${coreJsURL}/jquery.js"></script>
<script src="${coreJsURL}/jquery.quicksand.js"></script>
<script src="${resourceURL}/ares/jquery.tagfiltering.js"></script>

<style>
ul.tags {
	list-style-type: none;
	margin: 0;
	padding: 0;
}

ul.tags li {
	display: inline-block;
	/* margin-right: 4px; */
	margin-bottom: 4px;
}



.result {
	margin: 0;
	padding: 0;
}

.result li {
	text-align: left;
	list-style: none;
	line-height: 1.5em;
	padding: 10px;
	padding-left: 15px;
	margin-bottom: 10px;
	background: #fff;
	border: 1px solid #a5d24a;
}

.result li:hover {
	background: #ddf6bc;
}

.result .out {
	background: #fcf1f1;
	border: 1px solid #ff0000;
}

.result .out:hover {
	background: #f1d5d5;
}

.result li a {
	color: #353535;
	text-decoration: none;
}

.result li a:hover {
	font-weight: bold;
	text-decoration: underline;
}
</style>
<title>Search document</title>
</head>

<body>
<%@ include file="/WEB-INF/core/fragment/menu.jspf" %>
<%@ include file="/WEB-INF/ares/fragment/navigation.jspf" %>
<h1 id="banner">Search</h1>
<hr/><br/>
<%
String queryString = request.getParameter("q");
%>
<form class="search" action="search.html" method="GET">
<input type="text" name="q" value="<%=(queryString == null) ? "" : queryString%>" placeholder="enter searching keyword here" required/>
<button type="submit">Search</button>
</form>

<br/><br/>

<%
// print result
if (queryString != null && queryString != "") {
%>
<c:choose>
	<c:when test="${fn:length(profiles) gt 0}">
		<h4><s:message code="search.found" arguments="${fn:length(profiles)}"/>: <font color="#f00"><%=queryString%></font></h4>
		<!-- show tags -->
		<span id="allTag" class="active"><a class="tag" href="#">ALL ON</a></span>
		<ul class="tags">
			<c:forEach var="tag" items="${tags}" varStatus="i" begin="0">
				<li data-tag="${tags[i.index].name}" class="active"><a class="tag" href="#">${tags[i.index].name} x${tags[i.index].count}</a></li>
			</c:forEach>
		</ul>
		
		<!-- show documents -->
		<ul class="filter-items result">
		<!-- <ul id="docResult" class="result"> -->
			<c:forEach var="profile" items="${profiles}" varStatus="i" begin="0">
				<c:set var="id" value="${profiles[i.index].id}"/>
				<c:set var="score" value="${profiles[i.index].score}"/>

				<%-- <li data-id="${i.count}" data-tags="all, ${tagList}"> --%>
				<li data-id="${i.count}" data-tags="${tagList}">
					${i.count}. <a href="<c:url value="show.html"><c:param name="id" value="${id}"/></c:url>">${id}</a>, score: ${score}
					<%--<p style="margin-top:5px;margin-bottom:0"><small><i>${description}</i><br/>Tags: ${tagList}</small></p>--%>
				</li>
			</c:forEach>
		</ul>
	</c:when>
	<c:otherwise>
		<h4><s:message code="search.notFound"/>: <font color="#f00"><%=queryString%></font></h4>
	</c:otherwise>
</c:choose>
<%
}
%>
</body>
</html>