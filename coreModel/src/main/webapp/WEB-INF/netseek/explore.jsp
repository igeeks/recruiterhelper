<%@ include file="/WEB-INF/core/fragment/common.jspf" %>
<html>
<head>
<link rel="stylesheet" href="${coreCssURL}/core.css"/>
<title>Profile explorer</title>
</head>

<body>
<%@ include file="/WEB-INF/core/fragment/menu.jspf" %>
<%@ include file="/WEB-INF/netseek/fragment/navigation.jspf" %>
<h1 id="banner">List all documents</h1>
<hr/><br/>
<c:forEach items="${documents}" var="document" varStatus="status">
${status.count}. <a href="<c:url value="show.html"><c:param name="id" value="${document.id}"/></c:url>">${document.title}</a><br/>
</c:forEach>
</body>
</html>