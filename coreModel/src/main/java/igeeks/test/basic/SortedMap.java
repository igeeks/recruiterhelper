package igeeks.test.basic;

import igeeks.utils.Comparators;

import java.util.HashMap;
import java.util.TreeMap;

/**
 * @author TuanTA
 * @since 2014-08-01 17:05
 */
public class SortedMap {
	public static void main(String[] args) {
		HashMap<String, Float> unSoretedMap = new HashMap<String, Float>();
		Comparators.MapStringFloatComparator bvc = new Comparators.MapStringFloatComparator(unSoretedMap);
		TreeMap<String, Float> sorted_map = new TreeMap<String, Float>(bvc);

		unSoretedMap.put("D", new Float(67.3));
		unSoretedMap.put("A", new Float(99.5));
		unSoretedMap.put("B", new Float(67.4));
		unSoretedMap.put("C", new Float(67.5));
		unSoretedMap.put("E", new Float(99.5));

		sorted_map.putAll(unSoretedMap);

//		Object[] targetKeys={"D","B","C","E","A"};
//		Object[] currecntKeys=sorted_map.keySet().toArray();
//
//		assertArrayEquals(targetKeys,currecntKeys);

		System.out.println(unSoretedMap);
		System.out.println(sorted_map);
	}
}
