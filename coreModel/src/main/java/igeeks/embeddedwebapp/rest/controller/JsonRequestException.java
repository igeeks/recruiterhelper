package igeeks.embeddedwebapp.rest.controller;

import javax.servlet.ServletException;

/**
 * @author TuanTA
 * @since 2014-06-01 22:18
 */
@SuppressWarnings("serial")
public class JsonRequestException extends ServletException {
	public JsonRequestException(String msg) {
		super(msg);
	}

	public JsonRequestException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
