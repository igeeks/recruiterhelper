package igeeks.embeddedwebapp;

import org.mortbay.jetty.webapp.WebAppContext;
import org.springframework.context.ApplicationContext;

public class SpringWebappContext extends WebAppContext {
	ApplicationContext applicationContext;

	public SpringWebappContext(ApplicationContext appCtx) {
		super();
		applicationContext = appCtx;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
