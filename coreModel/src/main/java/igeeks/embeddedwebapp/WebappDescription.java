package igeeks.embeddedwebapp;

import org.springframework.context.ApplicationContext;

public class WebappDescription {
	String webappBase;
	String contextPath = "/";
	Integer port = null;
	String[] classpaths;
	String[] libs;
	String descriptorFile = null;
	ApplicationContext applicationContext;

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public String getWebappBase() {
		return webappBase;
	}

	public void setWebappBase(String webappBase) {
		this.webappBase = webappBase;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String[] getClasspaths() {
		return classpaths;
	}

	public void setClasspaths(String[] classpaths) {
		this.classpaths = classpaths;
	}

	public void setLibs(String[] libs) {
		this.libs = libs;
	}

	public String[] getLibs() {
		return libs;
	}

	public void setDescriptorFile(String descriptorFile) {
		this.descriptorFile = descriptorFile;
	}

	public String getDescriptorFile() {
		return descriptorFile;
	}
}
