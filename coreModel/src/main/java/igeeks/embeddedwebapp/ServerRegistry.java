package igeeks.embeddedwebapp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.handler.ContextHandlerCollection;
import org.mortbay.thread.ThreadPool;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class ServerRegistry implements InitializingBean, DisposableBean, ApplicationListener, ApplicationContextAware {

	static final Log LOG = LogFactory.getLog(ServerRegistry.class);

	boolean dumpBeforeStop = false;

	int gracefulShutdownTimeout = 60000;
	Server sharedServer;
	int sharedServerPort;
	int maxThreadPoolSize = 150;
	int startDelay = 1000 * 60;

	ContextHandlerCollection contextCollection = new ContextHandlerCollection();
	ApplicationContext applicationContext;

	public ServerRegistry(int sharedServerPort, int maxThreadPool) {
		this(sharedServerPort, new org.mortbay.thread.concurrent.ThreadPool(maxThreadPool));
		this.maxThreadPoolSize = maxThreadPool;
	}

	public ServerRegistry(int sharedServerPort, ThreadPool threadPool) {
		sharedServer = new Server(sharedServerPort);
		this.sharedServerPort = sharedServerPort;
		sharedServer.setThreadPool(threadPool);
		sharedServer.setHandler(contextCollection);

	}

	public void setStartDelay(int startDelay) {
		this.startDelay = startDelay;
	}

	public void setDumpBeforeStop(boolean dumpBeforeStop) {
		this.dumpBeforeStop = dumpBeforeStop;
	}

	public void setGracefulShutdownTimeout(int gracefulShutdownTimeout) {
		this.gracefulShutdownTimeout = gracefulShutdownTimeout;
	}

	public void setMaxThreadPoolSize(int maxThreadPoolSize) {
		this.maxThreadPoolSize = maxThreadPoolSize;
	}

	public void setLogLevel(String level) {
		System.setProperty("org.eclipse.jetty.LEVEL", level);// DEBUG|INFO
	}

	public Server deployWebapp(WebappDescription webappDescription, int threadPoolSize) throws Exception {
		return deployWebapp(webappDescription, new org.mortbay.thread.concurrent.ThreadPool(threadPoolSize));
	}

	public Server deployWebapp(String webappBase, String contextPath)
			throws Exception {
		return deployWebapp(null, webappBase, contextPath, maxThreadPoolSize);
	}

	public Server deployWebapp(ApplicationContext appctx, String webappBase, String contextPath) throws Exception {
		return deployWebapp(appctx, webappBase, contextPath, maxThreadPoolSize);
	}

	public Server deployWebapp(ApplicationContext appCtx, String webappBase, String contextPath, int poolsize) throws Exception {
		WebappDescription webappDescription = new WebappDescription();
		webappDescription.setWebappBase(webappBase);
		webappDescription.setContextPath(contextPath);
		webappDescription.setApplicationContext(appCtx);
		return deployWebapp(webappDescription, poolsize);
	}

	public Server deployWebapp(WebappDescription webappDescription) throws Exception {
		return deployWebapp(webappDescription, maxThreadPoolSize);
	}

	public Server deployWebapp(WebappDescription wad, ThreadPool threadPool) throws Exception {
		String classpath = "";
		Server server = sharedServer;

		if (wad.getPort() != null) {
			if (wad.getPort().intValue() == -1) {
				// temporary user want to disable this webapp
				return null;
			} else if (wad.getPort().intValue() != 0
					&& wad.getPort().intValue() != sharedServerPort) {
				server = new Server(wad.getPort());
				server.setThreadPool(threadPool);
			}
		}

		server.setStopAtShutdown(true);
		server.setGracefulShutdown(gracefulShutdownTimeout);

		if (wad.getClasspaths() != null) {
			for (String path : wad.getClasspaths()) {
				classpath += ";" + path;
			}
		}
		if (wad.getLibs() != null) {
			for (String path : wad.getLibs()) {
				File libPath = new File(path);
				if (libPath.exists() && libPath.isDirectory()) {
					for (File f : libPath.listFiles()) {
						classpath += (";" + f.getCanonicalPath());
					}
				}
			}
		}

		final SpringWebappContext webapp = new SpringWebappContext(wad.getApplicationContext() != null ? wad.getApplicationContext() : applicationContext);

		webapp.setExtraClasspath(classpath);

		if (wad.getWebappBase().endsWith(".war")) {
			webapp.setWar(wad.getWebappBase());
		} else {
			webapp.setResourceBase(wad.getWebappBase());
		}

		if (wad.getDescriptorFile() != null) {
			webapp.setDescriptor(wad.getDescriptorFile());
		}

		webapp.setContextPath(wad.getContextPath());
		webapp.setClassLoader(Thread.currentThread().getContextClassLoader());

		if (!sharedServer.equals(server)) {
			server.setHandler(webapp);
			server.start();
		} else {
			contextCollection.addHandler(webapp);
		}

		LOG.info("Deployed webapp " + wad.getWebappBase());
		return server;
	}

	public void destroy() throws Exception {
		try {
			sharedServer.stop();
		} catch (Throwable e) {
			LOG.debug("Fail to stop EmbeddedServer", e);
		}

	}

	public void onApplicationEvent(ApplicationEvent event) {

		if (event instanceof ContextRefreshedEvent) {

		}
	}

	private void loadWebApps() {
		if (sharedServer.isStarted() || !sharedServer.isRunning()) {
			try {
				sharedServer.stop();
				/*
				 * for (Handler handler:contextCollection.getHandlers()){ if
				 * (handler instanceof WebAppContext){ WebAppContext
				 * webapp=(WebAppContext)handler; //hijack current spring
				 * context into this webapp JettySpringContextLoaderListener
				 * listener=new JettySpringContextLoaderListener();
				 * 
				 * WebappDescription desc = this.mapHandlerDesc.get(handler);
				 * listener.setBeanFactory(desc!=null &&
				 * desc.getApplicationContext
				 * ()!=null?desc.getApplicationContext(
				 * ):applicationContext.getParent());
				 * webapp.addEventListener(listener);
				 * LOG.info("Loaded spring context to webapp " +
				 * webapp.getServletContext()+ " : " +
				 * applicationContext.getParent()); } }
				 */
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalStateException(
						"Can not stop sharedServer due to error "
								+ e.getMessage(), e);
			}
		} else {

		}
		start();
	}

	public void afterPropertiesSet() throws Exception {
		try {
//			SpringModule module = (SpringModule) applicationContext.getBean("mbvSpringModule");

			boolean notLoaded = false;
			/*if (module.getDependents() != null) {
				for (Module mod : module.getDependents()) {
					if (!mod.isLoaded()) {
						notLoaded = true;
						break;
					}
				}
			}*/

			// check if all dependent modules has been loaded
			if (notLoaded) {
				new Timer().schedule(new TimerTask() {
					@Override
					public void run() {
						try {
							afterPropertiesSet();
						} catch (Exception ex) {
							LOG.error("Error recalling afterPropertiesSet", ex);
						}
					}
				}, startDelay);
				return;
			}
		} catch (BeansException ex) {
			LOG.warn("Unexpected bean exception");
		}

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				loadWebApps();
			}
		}, startDelay);
	}

	public void start() {
		if (!sharedServer.isStarted() && !sharedServer.isStarting() && !sharedServer.isRunning()) {
			try {
				sharedServer.start();
				LOG.info("EmbeddedServer started at port " + sharedServerPort);
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalStateException("Can not start sharedServer due to error " + e.getMessage(), e);
			}
		} else {
			LOG.warn("Server is started");
		}
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;

		LOG.info("Set application context :" + applicationContext
				+ " ParentContext: " + applicationContext.getParent()
				+ " ParentbeanFactory:"
				+ applicationContext.getParentBeanFactory());
	}

}
