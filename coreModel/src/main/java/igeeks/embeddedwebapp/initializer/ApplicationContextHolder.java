package igeeks.embeddedwebapp.initializer;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Implementation of a Spring interface who is configured in Spring's applicationContext.xml or some
 * other Spring type of way. This class and the spring bean needed to wire it could be used as an
 * alternative to getting the ApplicationContext from the ServletContext.
 * @author TuanTA
 * @since 2014-08-01 13:02
 */
public class ApplicationContextHolder implements ApplicationContextAware {

	private static ApplicationContext appCtx;

	public ApplicationContextHolder() {
	}

	/** Spring supplied interface method for injecting app context. */
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		appCtx = applicationContext;
	}

	/** Access to spring wired beans. */
	public static ApplicationContext getApplicationContext() {
		return appCtx;
	}
}
