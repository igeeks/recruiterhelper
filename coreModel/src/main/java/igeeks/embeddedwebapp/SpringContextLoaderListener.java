package igeeks.embeddedwebapp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortbay.jetty.handler.ContextHandler;
import org.mortbay.jetty.servlet.Context.SContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;


/**
 * Class to replace {@link org.springframework.web.context.ContextLoaderListener} in web.xml
 * @author Nam Pham
 *
 */
public class SpringContextLoaderListener extends ContextLoaderListener {
	private static final Log LOG = LogFactory.getLog(SpringContextLoaderListener.class);
	
	@Override
	protected ContextLoader createContextLoader() {
		  return new DelegatingContextLoader();
	}
	
	public static class DelegatingContextLoader extends ContextLoader {

		@Override
		protected ApplicationContext loadParentContext(ServletContext servletContext) throws BeansException {
			return getWebAppContext(servletContext).getApplicationContext();
		}
		
	    @Override
		public WebApplicationContext initWebApplicationContext(ServletContext servletContext) throws IllegalStateException, BeansException {
			if (LOG.isDebugEnabled())
				LOG.debug("initializing servletContext " + servletContext.getServletContextName() + " " + servletContext.getContextPath());

			WebApplicationContext ctx = super.initWebApplicationContext(servletContext);
			if (ctx.getParent() == null) {
				LOG.info(ctx.getDisplayName() + " web application context has no parent assigning one");
				ApplicationContext parent = getWebAppContext(servletContext).getApplicationContext();
				if (ctx instanceof ConfigurableApplicationContext) {
					ConfigurableApplicationContext configurable = (ConfigurableApplicationContext) ctx;
					configurable.setParent(parent);
				} else throw new ApplicationContextException("applicationContext is not configurable");
			}
			return ctx;
		}

		SpringWebappContext getWebAppContext(ServletContext servletContext) throws BeansException {
			ContextHandler handler;
			if (servletContext instanceof SContext)
				handler = ((SContext) servletContext).getContextHandler();
			else
				throw new IllegalStateException("servletContext is of SContext from Jetty 6");

			if (handler instanceof SpringWebappContext)
				return (SpringWebappContext) handler;
			else
				throw new IllegalStateException("handler is not SpringWebAppContext; This handler can be only be used in web.xml of embedded-webapp module");

		}

		@Override
		protected WebApplicationContext createWebApplicationContext(ServletContext servletContext, ApplicationContext parent) throws BeansException {
			parent = parent != null ? parent : getWebAppContext(servletContext).getApplicationContext();
			return super.createWebApplicationContext(servletContext, parent);
		}
	}
}