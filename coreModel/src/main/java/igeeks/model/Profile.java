package igeeks.model;

import com.google.gson.Gson;
import com.mongodb.DBObject;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.List;

/**
 * @author TuanTA
 * @since 2014-07-24 17:34
 */
//@Entity(value = "profile_users")
public class Profile {
//	@Id
	String id;

	Name name;
	String url;
	String locality;
	String industry;
	String summary;

	List<String> skills;
	List<Education> education;
	List<Experience> experience;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	public List<Education> getEducation() {
		return education;
	}

	public void setEducation(List<Education> education) {
		this.education = education;
	}

	public List<Experience> getExperience() {
		return experience;
	}

	public void setExperience(List<Experience> experience) {
		this.experience = experience;
	}

	public Document toLuceneDocument() {
		Document doc = new Document();

		doc.add(new Field("id", id, Field.Store.YES, Field.Index.NOT_ANALYZED));

		if (name != null) {
			String fullname = name.getGivenName() + " " + name.getFamilyName();
			if (!name.equals(" "))
				doc.add(new Field("name", fullname, Field.Store.NO, Field.Index.ANALYZED));
		}

		if (locality != null)
			doc.add(new Field("locality", locality, Field.Store.NO, Field.Index.ANALYZED));

		if (industry != null)
			doc.add(new Field("industry", industry, Field.Store.NO, Field.Index.ANALYZED));

		if (experience != null) {
			StringBuffer s = new StringBuffer();
			for (Experience e : experience) {
				s.append((e.getTitle() == null ? "" : e.getTitle() + " ") +
						(e.getOrg() == null ? "" : e.getOrg() + " ") +
						(e.getDesc() == null ? "" : e.getDesc()));
			}
			if (s.length() > 0)
				doc.add(new Field("experience", s.toString(), Field.Store.NO, Field.Index.ANALYZED));
		}

		if (education != null) {
			StringBuffer s = new StringBuffer();
			for (Education e : education) {
				s.append((e.getName() == null ? "" : e.getName() + " ") +
						(e.getMajor() == null ? "" : e.getMajor() + " ") +
						(e.getDegree() == null ? "" : e.getDegree() + " ") +
						(e.getDesc() == null ? "" : e.getDesc() + " ") +
						(e.getActivities() == null ? "" : e.getActivities()));
			}
			if (s.length() > 0)
				doc.add(new Field("education", s.toString(), Field.Store.NO, Field.Index.ANALYZED));
		}

		if (skills != null && skills.size() > 0) {
			StringBuffer s = new StringBuffer();
			for (String skill : skills)
				s.append(skill + " ");

			doc.add(new Field("skills", s.toString(), Field.Store.NO, Field.Index.ANALYZED));
		}

		return doc;
	}
}
