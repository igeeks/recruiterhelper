package igeeks.model;

/**
 * @author TuanTA
 * @since 2014-07-24 17:48
 */
public class Name {
	String givenName;
	String familyName;

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
}
