package igeeks;

import igeeks.dao.LinkedinDAO;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

/**
 * @author TuanTA
 * @since 2014-07-23 16:55
 */
public class Launcher {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("ctx-app.xml");
		LinkedinDAO pdao = (LinkedinDAO) ctx.getBean("linkedinProfileDao");

		if (args.length > 0 && args[0].equals("index")) {
			pdao.indexAllProfiles();
			System.out.println("Finished indexing process!");
			System.exit(0);
//			pdao.indexProfiles(10000);
		} else {
//			pdao.printTop10Profiles();
//			pdao.indexTop10Profiles();

			String query = "Java C++";
			Map<String, Float> rs = pdao.search(query);
			if (rs != null) {
				System.out.println("Has " + rs.entrySet().size() + " results for query '" + query + "'");

				for (Map.Entry<String, Float> r : rs.entrySet())
					System.out.println(r.getKey() + " - " + r.getValue());
			}
		}
	}
}
