package igeeks.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Based json object adapter
 * @author TuanTA
 * @since 2014-06-01 22:49
 */
public abstract class JsonObjectAdapter<T> implements JsonDeserializer<T>, JsonSerializer<T> {

	protected abstract void marshal(JsonObject obj, T data, JsonSerializationContext context);

	protected abstract T unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException;

	@Override
	public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject obj = new JsonObject();

		marshal(obj, src, context);

		return obj;
	}

	@Override
	public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject data = (JsonObject) json;

		T obj = unmarshal(data, context);

		return obj;
	}
}
