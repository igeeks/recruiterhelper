package igeeks.json.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import igeeks.json.JsonObjectAdapter;
import igeeks.json.JsonUtils;
import igeeks.model.Name;

/**
 * @author TuanTA
 * @since 2014-07-25 12:57
 */
public class NameAdapter extends JsonObjectAdapter<Name> {
	@Override
	protected void marshal(JsonObject obj, Name data, JsonSerializationContext context) {
		if (data != null) {
			obj.add("family_name", JsonUtils.toJsonElement(data.getFamilyName()));
			obj.add("given_name", JsonUtils.toJsonElement(data.getGivenName()));
		}
	}

	@Override
	protected Name unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		Name name = null;

		if (obj != null) {
			name = new Name();
			name.setFamilyName(JsonUtils.getString(obj, "family_name"));
			name.setGivenName(JsonUtils.getString(obj, "given_name"));
		}

		return name;
	}
}
