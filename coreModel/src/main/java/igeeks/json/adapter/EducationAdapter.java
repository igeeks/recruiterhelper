package igeeks.json.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import igeeks.json.JsonObjectAdapter;
import igeeks.json.JsonUtils;
import igeeks.model.Education;

/**
 * @author TuanTA
 * @since 2014-07-25 12:56
 */
public class EducationAdapter extends JsonObjectAdapter<Education> {
	@Override
	protected void marshal(JsonObject obj, Education data, JsonSerializationContext context) {
		if (data != null) {
			obj.add("name", JsonUtils.toJsonElement(data.getName()));
			obj.add("degree", JsonUtils.toJsonElement(data.getDegree()));
			obj.add("major", JsonUtils.toJsonElement(data.getMajor()));
			obj.add("start", JsonUtils.toJsonElement(data.getStart()));
			obj.add("end", JsonUtils.toJsonElement(data.getEnd()));
			obj.add("desc", JsonUtils.toJsonElement(data.getDesc()));
			obj.add("activities", JsonUtils.toJsonElement(data.getActivities()));
		}
	}

	@Override
	protected Education unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		Education edu = null;

		if (obj != null) {
			edu = new Education();
			edu.setName(JsonUtils.getString(obj, "name"));
			edu.setDegree(JsonUtils.getString(obj, "degree"));
			edu.setMajor(JsonUtils.getString(obj, "major"));
			edu.setStart(JsonUtils.getString(obj, "start"));
			edu.setEnd(JsonUtils.getString(obj, "end"));
			edu.setDesc(JsonUtils.getString(obj, "desc"));
			edu.setActivities(JsonUtils.getString(obj, "activities"));
		}

		return edu;
	}
}
