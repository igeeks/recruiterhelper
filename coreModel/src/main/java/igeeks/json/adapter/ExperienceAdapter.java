package igeeks.json.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import igeeks.json.JsonObjectAdapter;
import igeeks.json.JsonUtils;
import igeeks.model.Experience;

/**
 * @author TuanTA
 * @since 2014-07-25 12:40
 */
public class ExperienceAdapter extends JsonObjectAdapter<Experience> {
	@Override
	protected void marshal(JsonObject obj, Experience data, JsonSerializationContext context) {
		if (data != null) {
			obj.add("title", JsonUtils.toJsonElement(data.getTitle()));
			obj.add("org", JsonUtils.toJsonElement(data.getOrg()));
			obj.add("start", JsonUtils.toJsonElement(data.getStart()));
			obj.add("end", JsonUtils.toJsonElement(data.getEnd()));
			obj.add("period", JsonUtils.toJsonElement(data.getPeriod()));
			obj.add("location", JsonUtils.toJsonElement(data.getLocation()));
			obj.add("desc", JsonUtils.toJsonElement(data.getDesc()));
		}
	}

	@Override
	protected Experience unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		Experience exp = null;

		if (obj != null) {
			exp = new Experience();
			exp.setTitle(JsonUtils.getString(obj, "title"));
			exp.setOrg(JsonUtils.getString(obj, "org"));
			exp.setStart(JsonUtils.getString(obj, "start"));
			exp.setEnd(JsonUtils.getString(obj, "end"));
			exp.setPeriod(JsonUtils.getString(obj, "period"));
			exp.setLocation(JsonUtils.getString(obj, "location"));
			exp.setDesc(JsonUtils.getString(obj, "desc"));
		}

		return exp;
	}
}
