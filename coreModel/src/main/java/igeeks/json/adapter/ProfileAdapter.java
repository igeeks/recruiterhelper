package igeeks.json.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.reflect.TypeToken;
import igeeks.json.JsonObjectAdapter;
import igeeks.json.JsonUtils;
import igeeks.model.Education;
import igeeks.model.Experience;
import igeeks.model.Name;
import igeeks.model.Profile;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author TuanTA
 * @since 2014-07-25 11:48
 */
public class ProfileAdapter extends JsonObjectAdapter<Profile> {

	@Override
	protected void marshal(JsonObject obj, Profile data, JsonSerializationContext context) {
		if (data != null) {
			obj.add("_id", JsonUtils.toJsonElement(data.getId()));
			obj.add("url", JsonUtils.toJsonElement(data.getUrl()));
			obj.add("locality", JsonUtils.toJsonElement(data.getLocality()));
			obj.add("industry", JsonUtils.toJsonElement(data.getIndustry()));

			Type type = new TypeToken<Name>(){}.getType();
			obj.add("name", context.serialize(data.getName(), type));

			type = new TypeToken<List<Education>>(){}.getType();
			obj.add("education", context.serialize(data.getEducation(), type));

			type = new TypeToken<List<Experience>>(){}.getType();
			obj.add("experience", context.serialize(data.getExperience(), type));
		}
	}

	@Override
	protected Profile unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		Profile profile = null;

		if (obj != null) {
			profile = new Profile();
			profile.setId(JsonUtils.getString(obj, "_id"));
			profile.setUrl(JsonUtils.getString(obj, "url"));
			profile.setLocality(JsonUtils.getString(obj, "locality"));
			profile.setIndustry(JsonUtils.getString(obj, "industry"));

			Type type = new TypeToken<Name>(){}.getType();
			profile.setName((Name) context.deserialize(obj.get("name"), type));

			type = new TypeToken<List<Education>>(){}.getType();
			profile.setEducation((List<Education>) context.deserialize(obj.get("education"), type));

			type = new TypeToken<List<Experience>>(){}.getType();
			profile.setExperience((List<Experience>) context.deserialize(obj.get("experience"), type));
		}

		return profile;
	}
}
