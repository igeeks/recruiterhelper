package igeeks.json.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import igeeks.json.JsonError;
import igeeks.json.JsonErrorSerializer;
import igeeks.json.adapter.EducationAdapter;
import igeeks.json.adapter.ExperienceAdapter;
import igeeks.json.adapter.NameAdapter;
import igeeks.json.adapter.ProfileAdapter;
import igeeks.model.Education;
import igeeks.model.Experience;
import igeeks.model.Name;
import igeeks.model.Profile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author TuanTA
 * @since 2014-07-25 11:04
 */
public class ProfileConverter {
	private final Log LOG = LogFactory.getLog(this.getClass());

	static final Gson GSON = new GsonBuilder()
			.registerTypeAdapter(JsonError.class, new JsonErrorSerializer())
			.registerTypeAdapter(Profile.class, new ProfileAdapter())
			.registerTypeAdapter(Name.class, new NameAdapter())
			.registerTypeAdapter(Education.class, new EducationAdapter())
			.registerTypeAdapter(Experience.class, new ExperienceAdapter())
			.create();

	public static Profile toProfile(String jsonObj) {
		return GSON.fromJson(jsonObj, Profile.class);
	}
}
