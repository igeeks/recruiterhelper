package igeeks.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class JsonErrorSerializer implements JsonSerializer<JsonError> {

	@Override
	public JsonElement serialize(JsonError src, Type typeOfSrc, JsonSerializationContext context) {
		Throwable cause = src.getCause();
		
		return serialize(cause);
	}

	JsonElement serialize(Throwable src) {
		JsonObject obj = new JsonObject();
		obj.add("class", JsonUtils.toJsonElement(src.getClass().getName()));
		obj.add("message", JsonUtils.toJsonElement(src.getMessage()));
		
		StringBuilder sb = new StringBuilder();
		
		for (StackTraceElement ste:src.getStackTrace()) {
			sb.append(ste.getClassName()).append(".")
			  .append(ste.getMethodName())
			  .append("() ")
			  .append(ste.getFileName())
			  .append(":")
			  .append(ste.getLineNumber())
			  .append("\n");
		}
		obj.add("trace", JsonUtils.toJsonElement(sb.toString()));
		
		Throwable cause = src.getCause();
		if (cause!=null && cause!=src)
			obj.add("cause", serialize(cause));
		return obj;
	}
}
