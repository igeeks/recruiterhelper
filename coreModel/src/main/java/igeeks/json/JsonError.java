package igeeks.json;

@SuppressWarnings("serial")
public class JsonError extends Exception {

	public JsonError(Throwable cause) {
		super("Just an error wrapper", cause);
	}
}