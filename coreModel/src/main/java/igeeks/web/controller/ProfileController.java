package igeeks.web.controller;

import igeeks.model.Profile;
import igeeks.web.service.ProfileService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author TuanTA
 * @since 2014-07-31 21:00
 */
@Controller
@RequestMapping("/netseek")
public class ProfileController {
	private final String VIEW_EXPLORE = "netseek/explore";
	private final String VIEW_SEARCH = "netseek/search";

	@Resource
	private ProfileService profileService;

	@RequestMapping(value = "explore", method = RequestMethod.GET)
	public ModelAndView explore() {
//		return new ModelAndView(VIEW_EXPLORE, "documents", profileService.findAllDocument());
		return null;
	}

	@RequestMapping(value = "search", method = RequestMethod.GET)
	public String search(HttpServletRequest request, Model model) {
		String searchString = request.getParameter("q");
		if (searchString == null || searchString == "")
			return VIEW_SEARCH;

		// add list doc to model
		Map<String, Float> profileMaps = profileService.search(searchString);
		if (profileMaps != null) {
			List<Profile> docs = new ArrayList<Profile>();
			for (Map.Entry<String, Float> e : profileMaps.entrySet()) {
				Profile p = new Profile();
				p.setId(e.getKey());
				docs.add(p);
			}
			model.addAttribute("profiles", docs.toArray(new Profile[docs.size()]));

		}

		return VIEW_SEARCH;
	}

}
