package igeeks.rest.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import igeeks.json.JsonObjectAdapter;
import igeeks.json.JsonUtils;
import igeeks.rest.model.CandidateRequest;

/**
 * @author TuanTA
 * @since 2014-08-01 14:20
 */
public class CandidateRequestAdapter extends JsonObjectAdapter<CandidateRequest> {
	@Override
	protected void marshal(JsonObject obj, CandidateRequest data, JsonSerializationContext context) {
		if (data != null) {
			obj.add("descKeywords", JsonUtils.toJsonElement(data.getDescKeywords()));
			obj.add("descFactor", JsonUtils.toJsonElement(data.getDescFactor()));
			obj.add("requirementKeywords", JsonUtils.toJsonElement(data.getRequirementKeywords()));
			obj.add("requirementFactor", JsonUtils.toJsonElement(data.getRequirementFactor()));
			obj.add("tags", JsonUtils.toJsonElement(data.getTags()));
			obj.add("tagsFactor", JsonUtils.toJsonElement(data.getTagFactor()));
			obj.add("limit", JsonUtils.toJsonElement(data.getLimit()));
			obj.add("threshold", JsonUtils.toJsonElement(data.getThreshold()));
		}
	}

	@Override
	protected CandidateRequest unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		CandidateRequest req = null;

		if (obj != null) {
			req = new CandidateRequest();

			req.setDescKeywords(JsonUtils.getString(obj, "descKeywords"));
			req.setDescFactor(JsonUtils.getFloat(obj, "descFactor"));
			req.setRequirementKeywords(JsonUtils.getString(obj, "requirementKeywords"));
			req.setRequirementFactor(JsonUtils.getFloat(obj, "requirementFactor"));
			req.setTags(JsonUtils.getString(obj, "tags"));
			req.setTagFactor(JsonUtils.getFloat(obj, "tagsFactor"));
			req.setLimit(JsonUtils.getInteger(obj, "limit"));
			req.setThreshold(JsonUtils.getFloat(obj, "threshold"));
		}

		return req;
	}
}
