package igeeks.rest.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import igeeks.json.JsonObjectAdapter;
import igeeks.json.JsonUtils;
import igeeks.rest.model.Candidate;

/**
 * @author TuanTA
 * @since 2014-08-01 14:38
 */
public class CandidateAdapter extends JsonObjectAdapter<Candidate> {

	@Override
	protected void marshal(JsonObject obj, Candidate data, JsonSerializationContext context) {
		if (data != null) {
			obj.add("id", JsonUtils.toJsonElement(data.getId()));
			obj.add("score", JsonUtils.toJsonElement(data.getScore()));
		}
	}

	@Override
	protected Candidate unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		Candidate candidate = null;

		if (obj != null) {
			candidate = new Candidate(JsonUtils.getString(obj, "id"), JsonUtils.getFloat(obj, "score"));
		}

		return candidate;
	}
}
