package igeeks.rest.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.reflect.TypeToken;
import igeeks.json.JsonObjectAdapter;
import igeeks.rest.model.Candidate;
import igeeks.rest.model.CandidateResponse;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author TuanTA
 * @since 2014-08-01 14:38
 */
public class CandidateResponseAdapter extends JsonObjectAdapter<CandidateResponse> {
	@Override
	protected void marshal(JsonObject obj, CandidateResponse data, JsonSerializationContext context) {
		if (data != null) {
			Type type = new TypeToken<List<Candidate>>(){}.getType();
			obj.add("byRequirement", context.serialize(data.getByRequirement(), type));
			obj.add("byDesc", context.serialize(data.getByDesc(), type));
			obj.add("byTags", context.serialize(data.getByTags(), type));
			obj.add("result", context.serialize(data.getResult(), type));
		}
	}

	@Override
	protected CandidateResponse unmarshal(JsonObject obj, JsonDeserializationContext context) throws JsonParseException {
		CandidateResponse resp = null;

		if (obj != null) {
			resp = new CandidateResponse();
			Type type = new TypeToken<List<Candidate>>(){}.getType();
			resp.setByRequirement((List<Candidate>) context.deserialize(obj.get("byRequirement"), type));
			resp.setByDesc((List<Candidate>) context.deserialize(obj.get("byDesc"), type));
			resp.setByTags((List<Candidate>) context.deserialize(obj.get("byTags"), type));
			resp.setResult((List<Candidate>) context.deserialize(obj.get("result"), type));
		}

		return resp;
	}
}
