package igeeks.rest.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import igeeks.json.JsonError;
import igeeks.json.JsonErrorSerializer;
import igeeks.rest.adapter.CandidateAdapter;
import igeeks.rest.adapter.CandidateRequestAdapter;
import igeeks.rest.adapter.CandidateResponseAdapter;
import igeeks.rest.model.Candidate;
import igeeks.rest.model.CandidateRequest;
import igeeks.rest.model.CandidateResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author TuanTA
 * @since 2014-08-01 13:27
 */
@SuppressWarnings("serial")
public abstract class AbstractJsonController extends HttpServlet {
	protected final Log LOG = LogFactory.getLog(this.getClass());

	private static final Gson GSON = new GsonBuilder()
			.registerTypeAdapter(JsonError.class, new JsonErrorSerializer())
			.registerTypeAdapter(Candidate.class, new CandidateAdapter())
			.registerTypeAdapter(CandidateRequest.class, new CandidateRequestAdapter())
			.registerTypeAdapter(CandidateResponse.class, new CandidateResponseAdapter())
			.create();

	/**
	 * for other purposes
	 * @return
	 */
	protected Gson gson() {
		return GSON;
	}

	protected void writeResponse(HttpServletResponse res, Object data) throws IOException {
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");

		String json = GSON.toJson(data);
		res.getWriter().append(json);
	}
}
