package igeeks.rest.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * @author TuanTA
 * @since 2014-08-01 13:26
 */
@SuppressWarnings("serial")
public class HelloController extends AbstractJsonController {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.writeResponse(resp, "Hi handsome boy, today is " + new Date());
	}
}
