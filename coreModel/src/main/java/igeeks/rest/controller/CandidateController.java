package igeeks.rest.controller;

import igeeks.embeddedwebapp.rest.controller.JsonRequestException;
import igeeks.embeddedwebapp.rest.web.RestContextHolder;
import igeeks.rest.model.Candidate;
import igeeks.rest.model.CandidateRequest;
import igeeks.rest.model.CandidateResponse;
import igeeks.service.ProfileService;
import igeeks.utils.Comparators;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author TuanTA
 * @since 2014-08-01 14:53
 */
public class CandidateController extends AbstractJsonController {
//	static final int MAX_CANDIDATE = 10;

//	LinkedinDAO profileDAO;
//	@Autowired
	ProfileService profileService;

	public CandidateController() {
//		profileDAO = RestContextHolder.getContext().getBean("linkedinProfileDao", LinkedinDAO.class);
		profileService = RestContextHolder.getContext().getBean("linkedinProfileService", ProfileService.class);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (!req.getContentType().contains("application/json")) {
			LOG.warn("The request is not in json format!");
			throw new JsonRequestException("expecting application/json request");
		}

		InputStreamReader reader = new InputStreamReader(req.getInputStream(), "UTF-8");
		try {
			CandidateRequest request = gson().fromJson(reader, CandidateRequest.class);
			CandidateResponse response = null;

			if (request != null) {
				Map<String, Float> byRequirement = queryCandidate(request.getRequirementKeywords());
				Map<String, Float> byDesc = queryCandidate(request.getDescKeywords());
				Map<String, Float> byTags = queryCandidate(request.getTags());
				Map<String, Float> result = composeResult(byRequirement, request.getRequirementFactor(),
														byDesc, request.getDescFactor(),
														byTags, request.getTagFactor());
				if (result != null) {
					response = new CandidateResponse();
					response.setByRequirement(mapToCandidate(byRequirement, request.getLimit(), request.getThreshold()));
					response.setByDesc(mapToCandidate(byDesc, request.getLimit(), request.getThreshold()));
					response.setByTags(mapToCandidate(byTags, request.getLimit(), request.getThreshold()));
					response.setResult(mapToCandidate(result, request.getLimit(), request.getThreshold()));
				}
			}

			writeResponse(resp, response);
		} catch (Exception e) {
			e.printStackTrace();
			throw new JsonRequestException("unexpected exception: " + e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (Throwable ignored) {
			}
		}
	}

	private Map<String, Float> queryCandidate(String query) {
		Map<String, Float> result = null;

		if (query != null && !query.isEmpty())
//			result = profileDAO.search(query);
			result = profileService.search(query);

		return result;
	}

	private Map<String, Float> composeResult(Map<String, Float> byRequirement, Float requirementFactor,
											 Map<String, Float> byDesc, Float descFactor,
											 Map<String, Float> byTags, Float tagFactor) {
		Map<String, Float> result = null;

		if (byRequirement != null || byDesc != null || byTags != null) {
			result = new HashMap<String, Float>();

			if (byRequirement != null)
				for (Map.Entry<String, Float> c : byRequirement.entrySet())
					addCandidateEntry(result, c, requirementFactor);

			if (byDesc != null)
				for (Map.Entry<String, Float> c : byDesc.entrySet())
					addCandidateEntry(result, c, descFactor);

			if (byTags != null)
				for (Map.Entry<String, Float> c : byTags.entrySet())
					addCandidateEntry(result, c, tagFactor);
		}

		return result;
	}

	private void addCandidateEntry(Map<String, Float> candidates, Map.Entry<String, Float> candidate, Float factor) {
		if (candidates.containsKey(candidate.getKey())) {
			candidates.put(candidate.getKey(), candidates.get(candidate.getKey()) + candidate.getValue() * factor);
		} else {
			candidates.put(candidate.getKey(), candidate.getValue() * factor);
		}
	}

	private List<Candidate> mapToCandidate(Map<String, Float> mapResult, Integer limit, Float threshold) {
		List<Candidate> candidates = null;

		if (mapResult != null && mapResult.entrySet().size() > 0) {
			Comparators.MapStringFloatComparator bvc = new Comparators.MapStringFloatComparator(mapResult);
			TreeMap<String, Float> sortedResult = new TreeMap<String, Float>(bvc);
			sortedResult.putAll(mapResult);

			candidates = new ArrayList<Candidate>();
			int count = 1;
			for (Map.Entry<String, Float> c : sortedResult.entrySet()) {
				candidates.add(new Candidate(c.getKey(), c.getValue()));

				if (++count > limit || c.getValue() < threshold) break;
			}
		}

		return candidates;
	}
}
