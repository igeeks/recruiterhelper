package igeeks.rest.model;

/**
 * @author TuanTA
 * @since 2014-08-01 14:18
 */
public class CandidateRequest {
	String descKeywords;
	Float descFactor;
	String requirementKeywords;
	Float requirementFactor;
	String tags;
	Float tagFactor;

	Integer limit;
	Float threshold;

	public String getDescKeywords() {
		return descKeywords;
	}

	public void setDescKeywords(String descKeywords) {
		this.descKeywords = descKeywords;
	}

	public Float getDescFactor() {
		return descFactor;
	}

	public void setDescFactor(Float descFactor) {
		this.descFactor = descFactor;
	}

	public String getRequirementKeywords() {
		return requirementKeywords;
	}

	public void setRequirementKeywords(String requirementKeywords) {
		this.requirementKeywords = requirementKeywords;
	}

	public Float getRequirementFactor() {
		return requirementFactor;
	}

	public void setRequirementFactor(Float requirementFactor) {
		this.requirementFactor = requirementFactor;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Float getTagFactor() {
		return tagFactor;
	}

	public void setTagFactor(Float tagFactor) {
		this.tagFactor = tagFactor;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Float getThreshold() {
		return threshold;
	}

	public void setThreshold(Float threshold) {
		this.threshold = threshold;
	}
}
