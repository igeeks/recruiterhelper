package igeeks.rest.model;

import java.util.List;

/**
 * @author TuanTA
 * @since 2014-08-01 14:32
 */
public class CandidateResponse {
	List<Candidate> byRequirement;
	List<Candidate> byDesc;
	List<Candidate> byTags;

	List<Candidate> result;

	public List<Candidate> getByRequirement() {
		return byRequirement;
	}

	public void setByRequirement(List<Candidate> byRequirement) {
		this.byRequirement = byRequirement;
	}

	public List<Candidate> getByDesc() {
		return byDesc;
	}

	public void setByDesc(List<Candidate> byDesc) {
		this.byDesc = byDesc;
	}

	public List<Candidate> getByTags() {
		return byTags;
	}

	public void setByTags(List<Candidate> byTags) {
		this.byTags = byTags;
	}

	public List<Candidate> getResult() {
		return result;
	}

	public void setResult(List<Candidate> result) {
		this.result = result;
	}
}
