package igeeks.rest.model;

/**
 * @author TuanTA
 * @since 2014-08-01 14:30
 */
public class Candidate {
	String id;
	Float score;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public Candidate(String id, Float score) {
		this.id = id;
		this.score = score;
	}
}
