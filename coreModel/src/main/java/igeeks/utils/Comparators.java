package igeeks.utils;

import java.util.Comparator;
import java.util.Map;

/**
 * @author TuanTA
 * @since 2014-08-01 17:10
 */
public class Comparators {
	public static class MapStringFloatComparator implements Comparator {
		Map<String, Float> base;

		public MapStringFloatComparator(Map<String, Float> base) {
			this.base = base;
		}

		/**
		 * Compare by DESC
		 * @param a
		 * @param b
		 * @return
		 */
		public int compare(Object a, Object b) {
			if ((Float) base.get(a) == (Float) base.get(b)) {
				return 0;
			} else if ((Float) base.get(a) > (Float) base.get(b)) {
				return -1;
			} else {
				return 1;
			}
		}
	}
}
