package igeeks.dao;

import igeeks.dto.ProfileDTO;
import org.apache.lucene.document.Document;

import java.util.HashMap;

/**
 * @author TuanTA
 * @since 2014-07-23 17:24
 */
public interface ProfileDAO {
	public void createIndex(Document profile);

	public void updateIndex(Document profile);

	public HashMap<String, Float> search(String queryString);
	public HashMap<String, Float> search(String queryString, int numResult);
	public HashMap<String, Float> search(String field, String queryString, int numResult);

	public void deleteIndex(String docId);
	public void deleteIndex(String field, String queryString);
}
