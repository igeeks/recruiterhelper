package igeeks.dao;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import igeeks.json.converter.ProfileConverter;
import igeeks.model.Education;
import igeeks.model.Experience;
import igeeks.model.Profile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author TuanTA
 * @since 2014-07-23 16:11
 */
public class LinkedinDAO implements ProfileDAO {
	protected final Log LOG = LogFactory.getLog(this.getClass());

	String mongoDomain;
	Integer mongoPort;
	String mongoSchema;
	String mongoCollection;

	String luceneIndexDir;

	private Version version = Version.LUCENE_35;
	private Analyzer analyzer = new StandardAnalyzer(version);

	public void setMongoDomain(String mongoDomain) {
		this.mongoDomain = mongoDomain;
	}

	public void setMongoPort(Integer mongoPort) {
		this.mongoPort = mongoPort;
	}

	public void setMongoSchema(String mongoSchema) {
		this.mongoSchema = mongoSchema;
	}

	public void setMongoCollection(String mongoCollection) {
		this.mongoCollection = mongoCollection;
	}

	public void setLuceneIndexDir(String luceneIndexDir) {
		this.luceneIndexDir = luceneIndexDir;
	}

	@Override
	public void createIndex(Document profile) {
		try {
			IndexWriter writer = getIndexWriter();
			writer.addDocument(profile);

//			writer.optimize();
			writer.close();
		} catch (CorruptIndexException e) {
			LOG.error("Index is corrupted in creating index: " + e.toString());
		} catch (LockObtainFailedException e) {
			LOG.error("File is locked in creating index: " + e.toString());
		} catch (IOException e) {
			LOG.error("IO error in creating index: " + e.toString());
		}
	}

	@Override
	public void updateIndex(Document profile) {
		Term key = new Term("id", profile.getFieldable("id").stringValue());

		try {
			IndexWriter writer = getIndexWriter();
			writer.updateDocument(key, profile);
			writer.close();
		} catch (IOException e) {
			LOG.error("IO error in updating index: " + e.toString());
		}
	}

	private IndexWriter getIndexWriter() throws IOException {
		File idxDir = new File(luceneIndexDir);
		if (!idxDir.exists() && idxDir.mkdir())
			throw new IOException("Cannot create directory for candidate index");

		Directory dir = FSDirectory.open(idxDir);
		IndexWriterConfig config = new IndexWriterConfig(version, analyzer);

		return new IndexWriter(dir, config);
	}

	@Override
	public HashMap<String, Float> search(String queryString) {
		return search(queryString, 100);
	}

	@Override
	public HashMap<String, Float> search(String queryString, int numResult) {
		try {
			String[] fields = new String[] { "industry", "experience", "education", "skills" };
			MultiFieldQueryParser queryParser = new MultiFieldQueryParser(version, fields, analyzer);
			Query query = queryParser.parse(queryString);

			return getResultMap(query, numResult);
		} catch (ParseException e) {
			LOG.error("Cannot parse query: " + e.toString());
		}

		return null;
	}

	@Override
	public HashMap<String, Float> search(String field, String queryString, int numResult) {
		try {
			QueryParser queryParser = new QueryParser(version, field, analyzer);
			Query query = queryParser.parse(queryString);

			return getResultMap(query, numResult);
		} catch (ParseException e) {
			LOG.error("Can not parse query: " + e.toString());
		}

		return null;
	}

	private HashMap<String, Float> getResultMap(Query query, int numResult) {
		HashMap<String, Float> ids = new HashMap<String, Float>();

		try {
			IndexReader reader = IndexReader.open(FSDirectory.open(new File(luceneIndexDir)));
			IndexSearcher searcher = new IndexSearcher(reader);
			TopScoreDocCollector collector = TopScoreDocCollector.create(numResult, true);
			searcher.search(query, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;

			for (int i = 0; i < hits.length; ++i) {
				Document d;
				d = searcher.doc(hits[i].doc);
				ids.put(d.get("id"), hits[i].score);
			}
		} catch (CorruptIndexException e) {
			LOG.error("IndexSearcher is corrupt: " + e.toString());
		} catch (IOException e) {
			LOG.error("Cannot open IO for query: " + e.toString());
		}

		return ids;
	}

	@Override
	public void deleteIndex(String profileId) {
		deleteIndex("id", profileId);
	}

	@Override
	public void deleteIndex(String field, String queryString) {
		Term term = new Term(field, queryString);
		try {
			IndexReader reader = IndexReader.open(FSDirectory.open(new File(luceneIndexDir)), false);
			reader.deleteDocuments(term);
			reader.close();
		} catch (IOException e) {
			LOG.error("IO error in deleting index: " + e.toString());
		}
	}

	/**
	 * Get top profiles by given amount
	 * @param maxAmount
	 * @return
	 */
	public List<DBObject> getTopProfiles(long maxAmount) {
		List<DBObject> result = null;

		if (maxAmount < 0)
			maxAmount = Long.MAX_VALUE;

		try {
			MongoClient mongo = new MongoClient(mongoDomain, mongoPort);
			DB db = mongo.getDB(mongoSchema);

			DBCollection collection = db.getCollection(mongoCollection);

			DBCursor cursorDoc = collection.find();

			result = new ArrayList<DBObject>();
			int count = 0;
			while (count < maxAmount && cursorDoc.hasNext()) {
//					System.out.println(cursorDoc.next());
				result.add(cursorDoc.next());
				count++;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}

		return result;
	}

	public List<DBObject> scanProfile(int skip, int limit) {
		List<DBObject> result = null;

		try {
			MongoClient mongo = new MongoClient(mongoDomain, mongoPort);
			DB db = mongo.getDB(mongoSchema);

			DBCollection collection = db.getCollection(mongoCollection);

			DBCursor cursorDoc = collection.find().skip(skip).limit(limit);

			result = new ArrayList<DBObject>();
			while (cursorDoc.hasNext()) {
				result.add(cursorDoc.next());
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}

		return result;
	}

	public void printTop10Profiles() {
		List<DBObject> objs = getTopProfiles(10);
		if (objs != null)
			for (DBObject obj : objs)
				System.out.println(obj);
	}

	public void indexProfiles(long amount) {
		List<DBObject> objs = getTopProfiles(amount);
		if (objs != null)
			for (DBObject obj : objs) {
				Profile p = ProfileConverter.toProfile(obj.toString());
				System.out.println(p.getId() + " - " + p.getUrl() + " - " + p.getLocality());
				System.out.println(p.getName().getFamilyName() + " - " + p.getName().getGivenName());
				if (p.getExperience() != null)
					for (Experience e : p.getExperience())
						System.out.println(e.getTitle() + " - " + e.getOrg());
				if (p.getEducation() != null)
					for (Education e : p.getEducation())
						System.out.println(e.getName() + " - " + e.getMajor() + " - " + e.getDegree());

				createIndex(p.toLuceneDocument());
				System.out.println("Created index for profile: " + p.getUrl());

				System.out.println("");
			}
	}

	public void indexTop10Profiles() {
		indexProfiles(10);
	}

	public void indexAllProfiles() {
//		indexProfiles(-1);
//		try {
//			MongoClient mongo = new MongoClient(mongoDomain, mongoPort);
//			DB db = mongo.getDB(mongoSchema);
//
//			DBCollection collection = db.getCollection(mongoCollection);
//
//			DBCursor cursorDoc = collection.find();
//
//			int count = 0;
//			while (cursorDoc.hasNext()) {
//				Profile p = ProfileConverter.toProfile(cursorDoc.next().toString());
//				createIndex(p.toLuceneDocument());
//				LOG.info("Created index for profile (num " + (++count) + "): " + p.getUrl());
//			}
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		} catch (MongoException e) {
//			e.printStackTrace();
//		}

		int count = 0;
		int skip = 0;
		int limit = 1000;
		while (true) {
			List<DBObject> objs = scanProfile(skip, limit);
			if (objs != null && objs.size() > 0) {
				skip += limit;

				for (DBObject obj : objs) {
					Profile p = ProfileConverter.toProfile(obj.toString());
					createIndex(p.toLuceneDocument());
					LOG.info("Created index for profile (num " + (++count) + "/" + skip + "): " + p.getUrl());
				}
			} else
				break;
		}
	}
}
