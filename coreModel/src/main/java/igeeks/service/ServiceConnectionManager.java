package igeeks.service;

/**
 * @author TuanTA
 * @since 2014-08-01 23:35
 */
public interface ServiceConnectionManager<T> {
	public T connectService() throws Exception;
	public void disconnectService(T service) throws Exception;
}
