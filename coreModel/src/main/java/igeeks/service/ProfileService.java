package igeeks.service;

import igeeks.dao.ProfileDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author TuanTA
 * @since 2014-08-01 23:39
 */
@Component("linkedinProfileService")
public class ProfileService implements ProfileDAO {
	protected final Log LOG = LogFactory.getLog(this.getClass());

	@Autowired
	@Qualifier("profilePool")
	ServicePool<ProfileDAO> profilePool;

	final long defaultAcquireTimeout = 10000;

	@Override
	public void createIndex(Document profile) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			profileDAO.createIndex(profile);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}

	@Override
	public void updateIndex(Document profile) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			profileDAO.updateIndex(profile);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}

	@Override
	public HashMap<String, Float> search(String queryString) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			return profileDAO.search(queryString);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
			return null;
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}

	@Override
	public HashMap<String, Float> search(String queryString, int numResult) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			return profileDAO.search(queryString, numResult);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
			return null;
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}

	@Override
	public HashMap<String, Float> search(String field, String queryString, int numResult) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			return profileDAO.search(field, queryString, numResult);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
			return null;
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}

	@Override
	public void deleteIndex(String docId) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			profileDAO.deleteIndex(docId);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}

	@Override
	public void deleteIndex(String field, String queryString) {
		ProfileDAO profileDAO = null;
		try {
			profileDAO = profilePool.acquire(defaultAcquireTimeout);
			profileDAO.deleteIndex(field, queryString);
		} catch (Exception e) {
			LOG.error("Cannot acquire profile service pool cause " + e.getMessage());
		} finally {
			if (profileDAO != null) {
				profilePool.release(profileDAO);
			}
		}
	}
}
