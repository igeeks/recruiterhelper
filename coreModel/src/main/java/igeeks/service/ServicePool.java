package igeeks.service;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;

/**
 * @author TuanTA
 * @since 2014-08-01 23:34
 */
public class ServicePool<T> implements /*ApplicationContextAware, */ BeanFactoryAware, InitializingBean, DisposableBean {
	protected static Log LOG = LogFactory.getLog(ServicePool.class);

	String beanId;
	int initialInstances;
	int maximumInstances;
	//ApplicationContext appCtx;

	ArrayList<T> lstPool;
	ArrayList<T> lstAcquired;
	BeanFactory beanFactory;

	Semaphore lock;

	ServiceConnectionManager<T> serviceConMgr;

	public ServicePool() {
		this(null, 0, 10);
	}

	public ServicePool(String beanId, int initial, int max) {
		setBeanId(beanId);
		setInitialInstances(initial);
		setMaximumInstances(max);
	}

	public T acquire() throws InterruptedException {
		return acquire(Long.MAX_VALUE);
	}

	/**
	 * Wait until the pool is available and acquire a service object from the pool.
	 *
	 * @return service object
	 * @throws InterruptedException
	 * @since 1.2.3
	 */
	public T acquire(long timeout) throws InterruptedException {
		T ret = null;
		Thread currentThread = Thread.currentThread();

		if (LOG.isDebugEnabled())
			LOG.debug("Trying to ACQUIRE " + beanId + " for " + currentThread + " ...");

		if (lock != null)
			if (!lock.tryAcquire(timeout, TimeUnit.MILLISECONDS))
				return null;

		try {
			ret = takeFromPool();
		} catch (Exception e) {
			if (lock != null)
				lock.release();
			throw new RuntimeException("Unable to connect to service", e);
		}
		return ret;
	}

	private synchronized T takeFromPool() throws Exception {
		Thread currentThread = Thread.currentThread();
		if ((maximumInstances <= 0 || (lstAcquired.size() < maximumInstances)) && lstPool.size() == 0) {
			lstPool.add(serviceConMgr.connectService());
		}
		int acquired = lstAcquired.size();
		if (LOG.isDebugEnabled())
			LOG.debug("ACQUIRING " + beanId + "(" + acquired + ") for " + currentThread);
		T ret = lstPool.remove(0);
		if (LOG.isDebugEnabled()) {
			if (lstAcquired.contains(ret))
				throw new RuntimeException("This " + beanId + " has been acquired before");
		}
		lstAcquired.add(ret);
		if (LOG.isDebugEnabled())
			LOG.debug(beanId + "(" + acquired + ") ACQUIRED for " + currentThread);


		return ret;
	}

	private synchronized void returnToPool(T service) {
		Thread currentThread = Thread.currentThread();
		int acquired = lstAcquired.size() - 1;
		if (LOG.isDebugEnabled())
			LOG.debug("RELEASING " + beanId + "(" + acquired + ")" + " for " + currentThread + " ...");
		boolean removed = lstAcquired.remove(service);
		if (!removed)
			throw new RuntimeException(beanId + " is not from this pool!");
		lstPool.add(service);
		if (LOG.isDebugEnabled())
			LOG.debug(beanId + "(" + acquired + ")" + " RELEASED for " + currentThread);
	}

	/**
	 * Return a service object to the pool
	 *
	 * @param service
	 */
	public void release(T service) {
		Thread currentThread = Thread.currentThread();
		if (LOG.isDebugEnabled())
			LOG.debug("Trying to RELEASE " + beanId + " for " + currentThread + " ...");
		this.returnToPool(service);
		if (lock != null)
			lock.release();
	}

	/*
	@Override
	public void setApplicationContext(ApplicationContext value)
			throws BeansException {
		// TODO Auto-generated method stub
		appCtx = value;
	}
	*/

	public void setServiceConnectionManager(ServiceConnectionManager<T> mgr) {
		serviceConMgr = mgr;
	}

	public String getBeanId() {
		return beanId;
	}

	public void setBeanId(String beanID) {
		this.beanId = beanID;
	}

	public int getInitialInstances() {
		return initialInstances;
	}

	public void setInitialInstances(int initialInstances) {
		if (lstPool != null)
			throw new IllegalStateException("pool is running");
		this.initialInstances = initialInstances;
	}

	public int getMaximumInstances() {
		return maximumInstances;
	}

	public void setMaximumInstances(int maxInstances) {
		if (lstPool != null)
			throw new IllegalStateException("pool is running");
		this.maximumInstances = maxInstances;
	}

	public int getAcquiredInstanceCount() {
		return lstAcquired.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		if (initialInstances < 0)
			throw new IllegalArgumentException("Invalid initialInstances: must be a non-negative interal.");
		if (beanFactory != null) {
			if (serviceConMgr != null)
				throw new IllegalArgumentException("beanFactory and serviceConnectionManager must not be set together!");

			if (beanId == null)
				throw new IllegalArgumentException("Invalid beanId: beanId must refer to a defined bean in application context.");

			serviceConMgr = new ServiceConnectionManager<T>() {
				@SuppressWarnings("unchecked")
				@Override
				public T connectService() throws Exception {
					T bean = (T) beanFactory.getBean(beanId);
					if (lstAcquired.contains(bean))
						throw new IllegalStateException(beanId + " is not scoped as prototype.");
					return bean;
				}

				@Override
				public void disconnectService(T service) throws Exception {
					if (beanFactory instanceof ConfigurableBeanFactory) {
						ConfigurableBeanFactory configBf = (ConfigurableBeanFactory) beanFactory;
						for (T bean : lstPool) {
							configBf.destroyBean(beanId, bean);
						}
					}
				}
			};
		} else if (serviceConMgr == null) {
			throw new IllegalArgumentException("Invalid beanFactory and serviceConnectionManager, either of these value must be set!");
		} else {
			// serviceConnMgr is set
			LOG.debug("serviceConMgr is set with " + serviceConMgr);
		}
		// creating lock
		if (maximumInstances > 0) {
			lock = new Semaphore(maximumInstances, false);
		} else {
			lock = null;
		}

		// create the pool
		lstPool = new ArrayList<T>(maximumInstances > 0 ? maximumInstances : 10);
		lstAcquired = new ArrayList<T>(maximumInstances > 0 ? maximumInstances : 10);

		// increase pool to initialInstances
		while (lstPool.size() < initialInstances) {
			lstPool.add(serviceConMgr.connectService());
		}

		LOG.info("Initialized ServicePool of " + beanId + " with " + lstPool.size() + " initial instances.");
	}

	@Override
	public void destroy() throws Exception {
		for (T bean : lstPool) {
			serviceConMgr.disconnectService(bean);
		}
		LOG.info("Destroying ServicePool of " + beanId + " with " + lstAcquired.size() + " instances occupied.");
		lstAcquired.clear();
		lstAcquired = null;

		lstPool.clear();
		lstPool = null;
	}

	@Override
	public void setBeanFactory(BeanFactory value) throws BeansException {
		// TODO Auto-generated method stub
		beanFactory = value;
	}

	public BeanFactory getBeanFactory() {
		return beanFactory;
	}
}
