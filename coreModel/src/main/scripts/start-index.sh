##
# Author: TuanTA
# Usage:
#   [path_to/app/bin/]start-update-dmcl.sh [log_dir]
#       + {log_dir} is optional, leave it blank if you want to store log at [path_to/app/]log
#
# Copyright 2014, Mobivi, Inc
# All rights reserved
##

logDir="$1" # optional
package="index"

home=`dirname "${BASH_SOURCE-$0}"`
home=`cd "$home/..">/dev/null; pwd`

. "$home"/bin/lib.sh
log="$(getLogDir ${home}/log ${logDir})/netseek.$package.$(getUser).$(getTime).log"

classpath=$home/conf:$home/lib/*

java -cp $classpath igeeks.Launcher index > "$log"

result=$?
if [ $result -ne 0 ]; then
	echo "$(date -u) Back-end module for netseek.$package did not terminate properly!" >> "$log"
	exit $result;
fi

echo "Done!" >> "$log"
exit 0;