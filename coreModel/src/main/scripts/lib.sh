##
# Author: TuanTA
# Usage:
#   . [path_to/]lib.sh
#
# Copyright 2014, Mobivi, Inc
# All rights reserved
##

# Usage:
#     getUser
getUser() {
    #echo $(whoami)
    echo $(id -u -n)
}

# Usage:
#     getTime
getTime() {
    echo $(date +"%y%m%d-%H%M%S")
}

# Usage:
#     existDir DIRECTORY
existDir() {
    [ "${1}" ] && [ -d "${1}" ] && return 0
    return 1
}

# Usage:
#     makeDir DIRECTORY
#        if you are not interested if successful or not
# or: makeDir DIRECTORY || return 1
#        if you want to stop further execution of a function after failure
# or: makeDir DIRECTORY || exit 1
#        if you want to stop the script after failure
makeDir() {
    [ -z "${1}" ] && return 1
    [ -d "${1}" ] || mkdir -p "${1}" 2>/dev/null || { echo "Error: cannot mkdir $1"; return 1; }
    return 0
}

# Usage:
#     getLogDir DIRECTORY
getLogDir() {
    curDir="${1}"
    logDir="${2}"
    existDir "${logDir}"
    if [ $? -eq 0 ]; then
        logDir=${logDir%/}
    else
        existDir "${curDir}"
        if [ $? -ne 0 ]; then makeDir "${curDir}" || exit 1; fi
        logDir="${curDir}"
    fi
    echo "${logDir}"
}

# Usage:
#     extractName FILENAME
extractName() {
    logName="${1##*/}"
    logName="${logName%.*}"
    echo "${logName}"
}

# Usage:
#     isNum $N && echo "yes"
#        do something if ok
# or: isNum $N || echo "no"
#        do something if not ok
isNum() {
    ksh -c "[[ \"$1\" = +([0-9]) ]]" return $?
}