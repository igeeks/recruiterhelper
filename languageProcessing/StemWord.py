from nltk.stem.snowball import SnowballStemmer

class StemWord:
    #languages are danish dutch english finnish french german hungarian italian
    #norwegian porter portuguese romanian russian spanish swedish
    def __init__(self,language):
        self._stemmer = SnowballStemmer(language)
    def stem(self,word):
        return self._stemmer.stem(word)    