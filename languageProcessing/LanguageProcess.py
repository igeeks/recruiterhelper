import StopWordFilter
import json
import re
import codecs
class LanguageProcess:
    def __init__(self,dataFileName,stopWordFileName,outputFileName):
        self._data = codecs.open(dataFileName,'r', encoding='utf-8')
        self._output = codecs.open(outputFileName,'w', encoding='utf-8')
        self._stopWord = StopWordFilter.englishStopWord(stopWordFileName)
        self._skipNonAlphaNumber = re.compile('[^a-zA-Z0-9]*(?P<string>[a-zA-Z0-9]+)[^a-zA-Z0-9]*')
    def _filter_stop_word(self,textRow,sourceField,destField):
        if sourceField not in textRow:
            return
        words = textRow[sourceField].split()
        rs = []
        for word in words:
            m = self._skipNonAlphaNumber.match(word)
            if m:
                word = m.group('string')
            word = word.lower()
            if not self._stopWord.is_stop_word(word) and word not in rs:
                rs.append(word)
        textRow[destField] = rs
        #print textRow[destField]
    def process_data(self):
        for row in self._data.readlines():
            textRow = json.loads(row)
            self._filter_stop_word(textRow,'description','description_keywords')
            self._filter_stop_word(textRow,'requirement','requirement_keywords')
            self._output.write(json.dumps(textRow)+'\n')
        self._output.close()

x = LanguageProcess('data.txt','/Users/lebahoang/work/recruiterhelper/languageProcessing/stopword.txt','jobs.txt')
x.process_data()
print 'OK'