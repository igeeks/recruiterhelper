import codecs
from StemWord import StemWord
class englishStopWord:
    def __init__(self,rawStopWordFile):
        f = codecs.open(rawStopWordFile, 'r', encoding='utf-8')
        self._stemmer = StemWord('english')
        self._stopwords = {}
        self._checked = {}
        for word in f:
            word = self._stemmer.stem(word.strip().lower())
            self._stopwords[word] = 1
    def is_stop_word(self,word):
        #if word in self._checked:
        #    return self._checked[word]
        #rs = False
        #for stopWord in self._stopwords.keys():
        #    if stopWord.find(word) != -1:
        #        rs = True 
        #        break
        #self._checked[word] = rs
        #return rs
        word = self._stemmer.stem(word.lower())
        if word in self._stopwords:
            return True
        return False
        
