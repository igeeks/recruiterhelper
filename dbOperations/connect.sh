mode="$1"

if [[ $mode && $mode = "install" ]]; then
  echo "Adding AWS key to RSA storage..."
  ssh-add tuanta.pem
fi

ssh -A ubuntu@ec2-54-179-134-29.ap-southeast-1.compute.amazonaws.com
