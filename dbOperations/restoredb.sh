#!/bin/sh

dbPath="$1"
db="$2"
collection="$3"
backupFolder="$4"

if [ -n "$dbPath" ]; then
  dbPath="--dbpath $dbPath"
fi
if [ -n "$db" ]; then
  db="--db $db"
 fi
if [ -n "$collection" ]; then
  collection="--collection $collection"
fi
sudo mongorestore $dbPath $db $collection $backupFolder