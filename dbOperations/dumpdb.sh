#!/bin/sh
dbPath=$1
outputFolder=$2
db=$3
collection=$4


if [ -n "$dbPath" ];
then
  echo $dbPath
  dbPath="--dbpath $dbPath"
fi
if [ -n "$outputFolder" ];
then
  outputFolder="--out $outputFolder"
fi  
if [ -n "$db" ];
then
  db="--db $db"
fi
if [ -n "$collection" ];
then
  collection="--collection $collection"
fi

sudo mongodump $dbPath $outputFolder $db $collection 