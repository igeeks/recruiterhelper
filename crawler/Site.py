import httplib
import HtmlParser
import string
import random
import urlparse 
import json
import time
import Utils
class Site:
    def __init__(self,toc_source_url):
        self._toc_source_url = toc_source_url
        set_url = [self._toc_source_url] if type(self._toc_source_url) is not list else self._toc_source_url
        self._path = []
        for url in set_url:
            url_parser_rs = urlparse.urlparse(url)
            self._path.append(url)
        self._scheme = urlparse.urlparse(set_url[0]).scheme
        self._domain = urlparse.urlparse(set_url[0]).netloc
        self._toc_file = "metadata.txt"
        self._thread_metadata_file = "threads_metadata.txt"
        self._thread_file = "threads.txt"
        self._data = "data.txt"

        self._next_page_parser = None
        self._thread_parser = None

   
    def _get_next_link(self,data):
        raise Exception("not implemented yet!")
    def _get_thread_links(self,data):
        raise Exception("not implemented yet!")
    def get_toc_page(self):
        print self._toc_file
        toc_file = open(self._toc_file,"w")
        client = httplib.HTTPConnection(self._domain)
        for url in self._path:
            print url
            while True:
                try:
                    client.request("GET", url)
                    res = client.getresponse()
                    data = res.read()
                    print res.status,url
                    page_name = "page_"+Utils.get_random_string()+".html"
                    page = open(page_name,"w")
                    page.write(data)
                    toc_file.write(page_name+"\n")
                    next_link = self._get_next_link(data)
                    if not next_link:
                        break
                    print next_link
                    url = next_link
                except Exception:
                    print "Error at page", url, "try again"
                    time.sleep(5)
    def parse_toc_page(self):
        toc_file = open(self._toc_file,"r")
        thread_metadata_file = open(self._thread_metadata_file,"w") 
        for filename in toc_file.readlines():
            f = open(filename.strip(),"r")
            links_to_job = self._get_thread_links(f.read())
            if links_to_job:
                for link in links_to_job:
                    thread_metadata_file.write(link.get("href",None)+"\n")
    def get_thread_page(self):
        thread_metadata_file = open(self._thread_metadata_file,"r")
        thread_file = open(self._thread_file,"w")
        client = httplib.HTTPConnection(self._domain)
        i = 0
        for thread_url in thread_metadata_file.readlines():
            try:
                print thread_url.strip()
                client.request("GET", thread_url.strip())
                res = client.getresponse()
                filename = "thread_"+str(i)+".html"
                f = open(filename,"w")
                f.write(res.read())
                f.close()
                thread_file.write(filename+"\n")
                i += 1
                #time.sleep(2)
            except Exception:
                print "exception", thread_url.strip()
                time.sleep(15)

    def parse_thread_page(self):
        raise Exception("not implemented yet!")