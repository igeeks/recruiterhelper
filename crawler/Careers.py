import httplib
import HtmlParser
import urlparse 
import json
import Site
import Utils
from bs4 import BeautifulSoup
class Careers(Site.Site):
    def __init__(self,toc_source_url):
        Site.Site.__init__(self,toc_source_url)
        self._next_page_parser = HtmlParser.HtmlParser(tags = ["a"])
        self._thread_parser = HtmlParser.HtmlParser(tags=["div","a"])
    def _get_next_link(self,data):
        root = self._next_page_parser.parse(data)
        next_link = root.find('a[@class=".*test-pagination-next.*"]')
        if next_link:
            return next_link[0].get("href",None)
        return None
        
    def _get_thread_links(self,data):
        root = self._thread_parser.parse(data)
        links_to_job = root.find('//div[@data-jobid=".*"]//a[@class="job-link"]')
        return links_to_job        
        
    def _get_job_title(self,root):
        job_title = root.find('//h1[@id="title"]/a[@class="title job-link"]')
        if job_title:
            return job_title[0].content()
        return "None"   
    def _get_employer(self,root):
        employer = root.find('//a[@class="employer"]')
        if employer:
            return employer[0].content()
        return "None"
    def _get_job_location(self,root):
        location = root.find('//span[@class="location"]')
        if location:
            return location[0].content()
        return "None"
    def _get_job_tags(self,root):
        tags = root.find('//p[@id="tags"]/a[@class="post-tag job-link"]')
        if tags:
            return [tag.content() for tag in tags]
        return []
    def _get_job_description_and_requirement(self,root):
        des = root.find('//div[@class="description"]')
        if des:
            requirement = root.find('//div[@class="jobdetail"]/h2')
            if requirement and requirement[0].content().strip() == "Skills &amp; Requirements":
                description = des[0].content().strip()
                if len(des) > 1:
                    h_index = des[1].content().find("<h2>")
                    requirement = des[1].content() if h_index == -1 else des[1].content()[:h_index]
                else:
                    requirement = "None"
                s = BeautifulSoup(description.strip())
                s1 = BeautifulSoup(requirement.strip())
                return s.get_text(), s1.get_text()
            else:
                str = ""
                for d in des[:2]:
                    h_index = d.content().find("</div")
                    if h_index != -1:
                        str += d.content()[:h_index] + "\n"
                    else:
                        str += d.content()
                s = BeautifulSoup(str.strip())
                return s.get_text(),"None"
        return "None","None"
    def _get_company_about(self,root):
        about = root.find('//div[@class="description"]')
        if about:
            h_index = about[-1].content().find("</div")
            return about[-1].content() if h_index == -1 else about[-1].content()[:h_index]
        return "None"
    def parse_thread_page(self):
        thread_file = open(self._thread_file,"r")
        data_file = open(self._data,"w")
        parser = HtmlParser.HtmlParser(tags=["div","h1","h2","a","span","p"])
        for thread in thread_file.readlines():
            f = open(thread.strip(),"r")
            root = parser.parse(f.read())
            job_title = self._get_job_title(root)
            employer = self._get_employer(root)
            job_location = self._get_job_location(root)
            job_tags = self._get_job_tags(root)
            job_des, requirement = self._get_job_description_and_requirement(root)
            about = self._get_company_about(root)

            obj = {"job_title": job_title, "employer": employer, 
                    "location": job_location, "tags": job_tags, 
                    "description":job_des, "requirement":requirement, "about":about, "domain":"tech"}
            data_file.write(json.dumps(obj)+"\n")
x = Careers("http://careers.stackoverflow.com/jobs?f=t")
#x.get_toc_page()
#x.parse_toc_page()
#x.get_thread_page()
x.parse_thread_page()
