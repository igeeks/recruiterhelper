class HtmlNode:
    def __init__(self, tag, start, end = None, attributes = dict()):
        import copy
        self._tag = tag
        self._children = []
        self._parent = None
        self._start = start
        self._end = end
        self._attributes = attributes
        for key,value in self._attributes.items():
            self._attributes[key] = value.strip()
        self._content = None
    def get(self, key, defaultValue = None):
        if self._attributes.has_key(key):
            return self._attributes[key]
        else:
            return defaultValue
    def has(self,key):
        return self._attributes.has_key(key)
    def attributes(self):
        return self._attributes
    def classes(self):
        return self.get('class', '').split()
    def has_class(self, cls):
        return cls in self.classes()
    def set_end(self, data, end):
        self._end = end
        self._content = data[self._start:self._end]
    def content(self):
        return self._content
    def span(self):
        return (self._start, self._end)
    def tag(self):
        return self._tag
    def add_child(self, child):
        self._children.append(child)
        child._parent = self
    def children(self):
        return self._children
    def parent(self):
        return self._parent
    def iterate_depth_first(self, function, depth = 0):
        function(self, depth)
        for child in self._children:
            child.iterate_depth_first(function, depth + 1)
    def _iterate_xpath(self, elements, function, depth):
        import re
        tag,attribute_name,attribute_value = elements[0]
        if tag == '':
            self._iterate_xpath(elements[1:], function, depth)
            for child in self.children():
                child._iterate_xpath(elements, function, depth + 1)

        for child in self.children():
            if (child.tag() == tag or tag == '*') and \
                    (not attribute_name or \
                         (attribute_name and not attribute_value and child.has(attribute_name)) or \
                         (attribute_name and attribute_value and child.get(attribute_name,None) \
                              and re.match(unicode(attribute_value), child.get(attribute_name), re.M | re.S))):
                if len(elements) == 1:
                    #print self.tag(),child.tag(),tag,child.get("class"),attribute_value,elements
                    #print 'z',child.tag(),depth
                    function(child, depth)
                else:
                    child._iterate_xpath(elements[1:], function, depth + 1)
    def _parse_xpath(self, tag):
        if not tag:
            return []
        elements = tag.split('/')
        xpath = []
        for element in elements:
            left_bracket = element.find('[')
            if left_bracket != -1:
                tag = element[:left_bracket]
                right_bracket = element.rfind(']')
                if right_bracket == -1:
                    raise Exception('Missing right bracket in "%s"' % element)
                attribute_spec = element[left_bracket+1:right_bracket]
                if attribute_spec.startswith('@') == -1:
                    raise Exception('Missing @ in "%s"' % element)
                attribute_spec = attribute_spec[1:]
                if attribute_spec.find('=') == -1:
                    attribute_name = attribute_spec
                    attribute_value = None
                else:
                    attribute_name,attribute_value = attribute_spec.split('=')
                    attribute_value = attribute_value.strip('"')
                    attribute_value = attribute_value.strip("'")
                    attribute_value = attribute_value.strip()
                    # add a trailing $ to ensure that we match exactly the requested regex
                    attribute_value = '%s$' % attribute_value
            else:
                tag = element
                attribute_name,attribute_value = None,None
            xpath.append(tuple([tag,attribute_name,attribute_value]))
        return xpath

    def find(self, tag):
        result = []
        if tag.find('/') != -1 or tag.find('[') != -1:
            elements = tag.split('//')
            xpath = self._parse_xpath(elements[0])
            for element in elements[1:]:
                xpath.append(tuple(['',None,None]))
                xpath.extend(self._parse_xpath(element)) 
                #print tag,xpath
            def accum(node, depth):
                result.append(node)
            self._iterate_xpath(xpath, accum, depth = 0)
        else:
            def accum(node, depth):
                if node.tag() == tag:
                    result.append(node)
            self.iterate_depth_first(accum, depth = 0)
        return result
    def str(self):
        output = []
        def accum(node, depth):
            output.append('  ' * depth + node.tag() + ' ' + str(node._attributes.items()))
        self.iterate_depth_first(accum)
        return '\n'.join(output)

class HtmlParser:
    def __init__(self, tags = ['table', 'td', 'tr', 'th', 'div']):
        self._tags = tags
        import re
        regex = '</?(%s)( [^>]*)?>' % '|'.join(['(%s)' % tag for tag in tags])
        self._regexp = re.compile(regex, re.I)
        self._attributes = re.compile("""([^= ]+?)=["']?([^"']+)["']?""")
    def _parse_html_tag_attribute(self, attributes_data):
        #attributes_data:  <a href="<%= items[i].url %>
        #print 'attributes_data: ', attributes_data
        attributes = attributes_data[1:]
        #get tag
        fs = attributes.find(' ')
        tag = attributes[:fs].strip().lower()
        #parse attributes
        attributes = attributes[fs+1:].strip()
        if not len(attributes):
            # we only have the tag
            return {'tag': tag, 'attributes': None}
        #parse the attributes
        eq = attributes.find('=')
        if eq < 0:
            # invalid html element
            return {'tag': tag, 'attributes': None}
        #href="http://www.tomsguide.fr" class="ib"
        res_attributes = []
        #print 'tag: ', tag
        while eq > 0:
            # get attribute name
            name = attributes[:eq].strip()
            #print 'name: ', name
            # get the attribute value and continue with others
            attributes = attributes[eq+1:].strip()
            #print 'attributes: ', attributes
            if attributes[0] not in ['\'', '"']:
                space_pos = attributes.find(' ')
                val = attributes[:space_pos].strip()
                attributes = attributes[space_pos+1:]
                res_attributes.append((name, val))
                eq = attributes.find('=')
                continue
            for delim in ['\'', '"']:
                if attributes[0] != delim:
                    continue
                delim_pos = 1
                a_seen = 1
                n_sign = 0
                #"http://www.tomsguide.fr" class="ib">
                while (a_seen % 2 != 0  or n_sign != 0) and delim_pos < len(attributes):
                    if attributes[delim_pos] == '<':
                        n_sign += 1
                    elif attributes[delim_pos] == '>':
                        n_sign -= 1
                    elif attributes[delim_pos] == delim:
                        a_seen += 1
                    delim_pos += 1  
                    
                if delim_pos > len(attributes) :
                    print 'Invalid attributes data: ', attributes_data
                    break   
                val = attributes[1:delim_pos - 1].strip()
                #print 'val: ', val
                attributes = attributes[delim_pos:]
                res_attributes.append((name, val))
                eq = attributes.find('=')
                break
                    
        return    {'tag': tag, 'attributes': res_attributes}
    def parse(self, data):
      
        matches = self._regexp.finditer(data)
        root = HtmlNode('root', start=0)
        root.set_end(data, len(data))
        stack = []
        for match in matches:
            if data[match.start() + 1] != '/':
                # start of a tag
                #attributes = self._attributes.findall(data[match.start():match.end()])
                #tag = match.group(1).lower()
                
                parsed_html_tag = self._parse_html_tag_attribute(data[match.start():match.end()])
                attributes = parsed_html_tag['attributes']
                tag = parsed_html_tag['tag']
                
                #print 'data: ', data[match.start():match.end()]
                #print '\n\n my attributes: ', self._parse_html_tag_attribute(data[match.start():match.end()])['attributes']
                #print '\n\n old attributes: ', attributes
                
                
                node = HtmlNode(tag, start=match.end(), attributes = dict((k.strip(), v.strip()) for k,v in attributes) if attributes is not None else {})
                if stack:
                    stack[-1].add_child(node)
                else:
                    root.add_child(node)
                stack.append(node)

            if data[match.start() + 1] == '/' or data[match.end() - 2] == '/':
                # end of a tag
                tag = match.group(1).lower()
                if stack and stack[-1].tag() == tag:
                    node = stack.pop()
                    node.set_end(data, match.start())
                else:
                    # if we are closing a tag that is not the currently 
                    # open tag, we ignore it. 
                    # these are bugs in the HTML that we work around.
                    #print 'ignoring %s' % match.group(1)
                    pass
        # now, 'close' all remaining tags. Clearly, there should be no remaining
        # tags if they were balanced but, in practice, they often are not.
        while stack:
            node = stack.pop()
            node.set_end(data, len(data))
        return root

def test_xpath():
    html = """<html><body>
<k><t></t><a></a></k>
<k><t x="foo"></t><a></a></k>
<k><t x='bar'></t><a></a></k>
<k><k z='zoo'><t x="bar"></t><a></a></k></k>
<c l='zoo lion' style="noo"></c>
<t></t>
</body></html>
"""
    def assert_find_n(path,expected):
        n = 0
        for i in root.find(path):
            n += 1
        assert n == expected

    parser = HtmlParser(tags=['k','t','c'])
    root = parser.parse(html)
    assert_find_n('k/t', 3)
    assert_find_n('k/t[@x="foo"]', 1)
    assert_find_n('k/t[@x="bar"]', 1)
    assert_find_n("k/t[@x='foo']", 1)
    assert_find_n("k/t[@x]", 2)
    assert_find_n('k/*', 4)
    assert_find_n('k/*/t', 1)
    assert_find_n('k/*[@z]/t', 1)
    assert_find_n('//t', 5)
    assert_find_n('k//t', 4)
    assert_find_n('c[@l="zoo lion"]', 1)


    parser = HtmlParser(tags=['b'])
    root = parser.parse(html)
    assert_find_n('b', 0)
    print 'xpath tests are OK'
    
def test_infosdunet_url_extraction():
    import re
    import base64
    content = """<a href="#" onmouseover="BOM.Utils.decodeLive('aHR0cDovL3d3dy5pbmZvcy1kdS1uZXQuY29tL2ZvcnVtLzMwNzQtNDAtb2ZmaWNpZWwtc3VwcG9ydC1saWdodGludGhlYm94L3BhZ2UtMg==', this);"
 class="crLink"><span class="ib picto next"></span>Suivant</a>"""
    parser = HtmlParser(tags=['a', 'span'])
    root = parser.parse(content)
    next_link = root.find('//a[@class="crLink"]/span[@class="ib picto next"]')
    if not next_link:
        print 'Test failed: no next link'
    href_val = next_link[0].parent().get('onmouseover')
    user_url_regex = re.compile("decodeLive[(]'([^']+?)',")
    m = user_url_regex.search(href_val)
    decoded_url = ''
    if m:
        decoded_url = base64.b64decode(m.group(1))
    assert(decoded_url == 'http://www.infos-du-net.com/forum/3074-40-officiel-support-lightinthebox/page-2')
    print 'infosdunet url extraction and decoding is OK'
def run_tests():
    test_xpath()
    test_infosdunet_url_extraction()


if __name__ == '__main__':
    run_tests()
