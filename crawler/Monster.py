import HtmlParser
import urlparse 
import json
import Site
import Utils
from bs4 import BeautifulSoup
class Monster(Site.Site):
    def __init__(self,toc_source_url):
        Site.Site.__init__(self,toc_source_url)
        self._next_page_parser = HtmlParser.HtmlParser(tags = ["a"])
        self._thread_parser = HtmlParser.HtmlParser(tags=["div","a"])
    def _get_next_link(self,data):
        root = self._next_page_parser.parse(data)
        next_link = root.find('a[@class=".*ns_pagenav_right.*"]')
        if next_link:
            return "/"+next_link[0].get("href",None)
        return None
        
    def _get_thread_links(self,data):
        root = self._thread_parser.parse(data)
        links_to_job = root.find('//div[@class="ns_job_wrapper"]//a[@class="ns_joblink"]')
        return links_to_job

    def _get_summary(self,root):
        return root.find('//div[@class="ns_jobsum_txt"]')
    def _get_job_title(self,root):
        title = root.find('//div[@class="ns_jd_headingbig hl"]')
        if title:
            return title[0].content().strip()
        return "None"
    def _get_required_experience(self,job_sum):
        if len(job_sum) >= 3:
            return job_sum[2].content().strip()
        return "None"
    def _get_employer(self,job_sum):
        if len(job_sum) >= 1:
            return job_sum[0].content().strip()
        return "None"
    def _get_job_location(self,job_sum):
        if len(job_sum) >= 2:
            return job_sum[1].content().strip()
        return "None"
    def _get_job_tags(self,root):
        tags = root.find('//a[@class="ns_jd_keyword"]')
        if tags:
            return [tag.content().strip() for tag in tags]
        return []
    def _get_job_description(self,root):
        des = root.find('//div[@class="ns_fulljd_wrap"]//div[@class="ns_jobdesc hl"]')
        if des:
            soup = BeautifulSoup(des[0].content().strip(),from_encoding='utf-8')
            return soup.get_text().encode('utf-8')
        return "None"
    def _get_company_about(self,root):
        about = root.find('//div[@class="ns_fulljd_wrap"]//div[@class="ns_jobdesc hl"]')
        if about:
            soup = BeautifulSoup(about[-1].content().strip().replace("."," "))
            return soup.get_text().encode('utf-8')
            #return about[-1].content().strip()
        return "None"
    def parse_thread_page(self):
        thread_file = open(self._thread_file,"r")
        data_file = open(self._data,"w")
        parser = HtmlParser.HtmlParser(tags=["div","h1","h2","h3","a","span","p","ul","li"])
        for thread in thread_file.readlines():
            f = open(thread.strip(),"r")
            root = parser.parse(f.read())
            job_sum = self._get_summary(root)
            job_title = self._get_job_title(root)
            experience = self._get_required_experience(job_sum)
            employer = self._get_employer(job_sum)
            job_location = self._get_job_location(job_sum)
            job_tags = self._get_job_tags(root)
            job_des = self._get_job_description(root)
            about = self._get_company_about(root)

            obj = {"job_title": job_title, "employer": employer, 
                    "location": job_location, "experience": experience, "tags": job_tags, 
                    "description":job_des, "about":about, "domain":"tech"}
            data_file.write(json.dumps(obj)+"\n")
            
        data_file.close()   

#x = Monster("http:\/\/jobsearch.monster.com.sg/searchresult.html?ind=32")
#x.get_toc_page()
#x.parse_toc_page()
#x.get_thread_page()
#x.parse_thread_page()
y = Monster("http://jobsearch.monsterindia.com/searchresult.html?ind=32")
#y.get_toc_page()
#y.parse_toc_page()
#y.get_thread_page()
y.parse_thread_page()
