import string
import random
def get_random_string():
    return ''.join(random.choice(string.ascii_uppercase) for i in range(20))